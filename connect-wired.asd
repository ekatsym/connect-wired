(defsystem "connect-wired"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("cl-cuda"
               "mgl-mat"
               "alexandria")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "connect-wired/tests"))))

(defsystem "connect-wired/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("connect-wired"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for connect-wired"

  :perform (test-op (op c) (symbol-call :rove :run c)))
